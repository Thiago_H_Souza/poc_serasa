package br.com.experian.poc.core.model;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Consumer {

	@Id
	private String id;
	private String name;
}
