package br.com.experian.poc.core.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.experian.poc.core.model.EventRegister;

public interface EventRegisterRepository extends MongoRepository<EventRegister, String> {

}