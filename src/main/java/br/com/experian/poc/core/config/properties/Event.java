package br.com.experian.poc.core.config.properties;

import lombok.Data;

@Data
public class Event{
	
	private String event;
	private String description;
	private Method method;
	
	enum Method{
		GET, POST, PUT, DELETE;
	}
}
