package br.com.experian.poc.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.experian.poc.core.config.properties.ApplicationProperties;
import br.com.experian.poc.core.config.properties.Event;
import br.com.experian.poc.core.model.Webhook;
import br.com.experian.poc.core.repository.WebhookRepository;

@Service
public class WebhookService {

	private WebhookRepository repository;
	private List<Event> eventList;

	@Autowired
	public WebhookService(WebhookRepository repository, ApplicationProperties properties) {
		this.repository = repository;
		this.eventList = properties.getEvents();

	}

	public Webhook register(Webhook wh) {
		verifyEvent(wh.getEvent());
		return repository.save(wh);
	}

	 public void unregister(String idConsumer, String event) {
		 repository.deleteByEventAndConsumer_id(idConsumer, event);
	 }

	public List<Webhook> findByEvent(String event) {
		return repository.findByEvent(event);
	}

	private void verifyEvent(String event) {
		eventList
			.stream()
			.filter(ev -> ev.getEvent().equalsIgnoreCase(event))
			.findFirst()
			.orElseThrow(IllegalArgumentException::new);
	}

	public List<Webhook> findAll() {
		return repository.findAll();
	}
}
