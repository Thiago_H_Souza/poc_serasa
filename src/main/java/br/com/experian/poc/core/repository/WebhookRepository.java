package br.com.experian.poc.core.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.experian.poc.core.model.Webhook;

public interface WebhookRepository extends MongoRepository<Webhook, String>{
	
	public List<Webhook> findByEvent(String event); 
	public Webhook deleteByEventAndConsumer_id(String idConsumer, String event);

}