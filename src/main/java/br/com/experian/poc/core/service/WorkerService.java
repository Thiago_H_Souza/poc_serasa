package br.com.experian.poc.core.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.experian.poc.core.model.Webhook;

@Component
public class WorkerService {
	
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private WebhookService service;

	public void handle(String event, Map<String, Object> payload) {

		List<Webhook> whList = service.findByEvent(event);

		whList.stream().forEach(webhook -> {
			this.restTemplate.getForObject(webhook.getUrl(), String.class);
		});

	}

}