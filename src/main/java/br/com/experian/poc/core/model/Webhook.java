package br.com.experian.poc.core.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Webhook {
	
	@Id
	private String id;
	
	private String event;
	
	@DBRef
	private Consumer consumer;
	
	@Indexed
	private String url;
		
}
