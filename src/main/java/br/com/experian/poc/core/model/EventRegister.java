package br.com.experian.poc.core.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class EventRegister {

	@Id
	private String id;
	private String event;
	private LocalDateTime in;
}
