package br.com.experian.poc.api.v1;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.experian.poc.core.model.EventRegister;
import br.com.experian.poc.core.repository.EventRegisterRepository;

@RestController
@RequestMapping("event")
public class EventApi {
	
	@Autowired
	private EventRegisterRepository repository;
	
	@GetMapping("user_created")
	public void userCreated() {
		System.out.println("USER_CREATED : " + LocalDateTime.now());
		save("USER_CREATED");		
	}

	@GetMapping("user_deleted")
	public void userDeleted() {
		System.out.println("USER_DELETED : " + LocalDateTime.now());
		save("USER_DELETED"); 
	}
	
	@GetMapping("user_changed")
	public void userChanged() {
		System.out.println("USER_CHANGED : " + LocalDateTime.now());
		save("USER_CHANGED");
	}
	
	private void save(String evento) {
		EventRegister ev = new EventRegister();
		ev.setEvent(evento);
		ev.setIn(LocalDateTime.now());
		repository.save(ev);
	}
}
