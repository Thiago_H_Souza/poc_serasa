package br.com.experian.poc.api.v1;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.experian.poc.core.service.WorkerService;

@CrossOrigin
@RestController
@RequestMapping("worker")
public class Worker {
	
	@Autowired
	WorkerService service;

	@PostMapping("/{event}")
	public void worker( @PathVariable("event") String event, @RequestBody Map<String, Object> payload) {
		service.handle(event, payload);
	}
}
