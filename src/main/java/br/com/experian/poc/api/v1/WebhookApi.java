package br.com.experian.poc.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.experian.poc.core.model.Webhook;
import br.com.experian.poc.core.service.WebhookService;

@CrossOrigin
@RestController
@RequestMapping("webhook")
public class WebhookApi {
	
	@Autowired
	WebhookService service;
	
	@PostMapping
	public Webhook register( @RequestBody Webhook webhook ) {
		return service.register(webhook);
	}
	
	@DeleteMapping("{idConsumer}/{event}")
	public void unregister(  @PathVariable("idConsumer") String idConsumer, @PathVariable("event") String event ) {
		service.unregister(idConsumer, event);
	}
	
	@GetMapping
	public List<Webhook> getAll() {
		return service.findAll();
	}

}
