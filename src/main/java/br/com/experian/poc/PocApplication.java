package br.com.experian.poc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.experian.poc.core.config.properties.ApplicationProperties;
import br.com.experian.poc.core.model.Webhook;
import br.com.experian.poc.core.repository.WebhookRepository;

@SpringBootApplication
public class PocApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocApplication.class, args);
	}
	
	@Bean
	CommandLineRunner run(ApplicationProperties prop, WebhookRepository repository) {
		return (evt) -> {
			save("USER_CHANGED", repository);
			save("USER_CREATED", repository);
			save("USER_DELETED", repository);
			
			
			System.out.println(repository.findAll());
			
			System.out.println(prop);
			
			
		};
	}
	
	private void save(String event, WebhookRepository repository) {
		Webhook wh = new Webhook();			
		wh.setEvent(event);
		wh.setUrl("https://http://openshift-jee-sample-teste-spring-boot.1d35.starter-us-east-1.openshiftapps.com/event/" + event.toLowerCase());
		
		repository.save(wh);
		System.out.println(wh);
	}
}
